import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/home",
      name: "home",
      component: () => import("./views/home.vue")
    },
    {
      path: "/products",
      name: "products",
      component: () => import("./views/products/products.vue")
    },
    {
      path: "/products/register",
      name: "products-register",
      component: () => import("./views/products/register.vue")
    },
    {
      path: "/products/inventory",
      name: "products-inventory",
      component: () => import("./views/products/inventory.vue")
    },
    {
      path: "/reports",
      name: "reports",
      component: () => import("./views/reports/reports.vue")
    },
    {
      path: "/recipes",
      name: "recipes",
      component: () => import("./views/recipes/recipes.vue")
    },
    {
      path: "/recipes-history",
      name: "recipes-history",
      component: () => import("./views/recipes/recipesHistory.vue")
    },
    {
      path: "/pos/sales",
      name: "sales",
      component: () => import("./views/pos/sales.vue")
    }
  ]
});
