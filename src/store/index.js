import Vue from "vue";
import Vuex from "vuex";
import { state } from "./state";
import { getters } from "./getters";
import { mutations } from "./mutations";
import { actions } from "./actions";
import { auth } from "./modules/auth";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth: auth
  },
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions,
  plugins: [
    createPersistedState({
      paths: ["auth"]
    })
  ]
});
