export const mutations = {
  changeDrawerState(state, $isMobile) {
    if (state.drawer == null && !$isMobile) {
      state.drawer = false;
    } else if (state.drawer == null && $ismobile) {
      state.drawer = true;
    } else {
      state.drawer = !state.drawer;
    }
  }
};