export const auth = {
  state: {
    userEmail: null,
    token: null
  },
  getters: {
    userEmail(state) {
      return state.userEmail;
    },
    token(state) {
      return state.token;
    }
  },
  mutations: {
    setUserData(state, payload) {
      state.userEmail = payload.userEmail;
      state.token = payload.token;
    }
  },
  actions: {}
}
